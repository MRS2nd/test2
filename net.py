# -*- coding: utf-8 -*-

import torch
import torch.nn as nn
import torch.nn.functional as F

# For model visualize
# Requirements torchviz, graphviz, tensorboard
# from torchviz import make_dot
# from torch.utils.tensorboard import SummaryWriter


class AutoEncoder(nn.Module):
    def __init__(self):
        super(AutoEncoder, self).__init__()
        channels = [64, 128, 256, 512, 512]

        # Encoder
        self.conv1_1 = nn.Conv2d(3, channels[0], 3, 1, 1)
        self.conv1_2 = nn.Conv2d(channels[0], channels[0], 3, 1, 1)
        self.conv2_1 = nn.Conv2d(channels[0], channels[1], 3, 1, 1)
        self.conv2_2 = nn.Conv2d(channels[1], channels[1], 3, 1, 1)
        self.conv3_1 = nn.Conv2d(channels[1], channels[2], 3, 1, 1)
        self.conv3_2 = nn.Conv2d(channels[2], channels[2], 3, 1, 1)
        self.conv3_3 = nn.Conv2d(channels[2], channels[2], 3, 1, 1)
        self.conv4_1 = nn.Conv2d(channels[2], channels[3], 3, 1, 1)
        self.conv4_2 = nn.Conv2d(channels[3], channels[3], 3, 1, 1)
        self.conv4_3 = nn.Conv2d(channels[3], channels[3], 3, 1, 1)
        self.conv5_1 = nn.Conv2d(channels[3], channels[4], 3, 1, 1)
        self.conv5_2 = nn.Conv2d(channels[4], channels[4], 3, 1, 1)

        self.bn1_1 = nn.BatchNorm2d(channels[0])
        self.bn1_2 = nn.BatchNorm2d(channels[0])
        self.bn2_1 = nn.BatchNorm2d(channels[1])
        self.bn2_2 = nn.BatchNorm2d(channels[1])
        self.bn3_1 = nn.BatchNorm2d(channels[2])
        self.bn3_2 = nn.BatchNorm2d(channels[2])
        self.bn3_3 = nn.BatchNorm2d(channels[2])
        self.bn4_1 = nn.BatchNorm2d(channels[3])
        self.bn4_2 = nn.BatchNorm2d(channels[3])
        self.bn4_3 = nn.BatchNorm2d(channels[3])
        self.bn5_1 = nn.BatchNorm2d(channels[4])
        self.bn5_2 = nn.BatchNorm2d(channels[4])

        # Decoder
        self.conv5_3 = nn.Conv2d(channels[4], channels[4], 3, 1, 1)
        self.conv6_1 = nn.Conv2d(channels[4], channels[3], 3, 1, 1)
        self.conv6_2 = nn.Conv2d(channels[3], channels[3], 3, 1, 1)
        self.conv6_3 = nn.Conv2d(channels[3], channels[3], 3, 1, 1)
        self.conv7_1 = nn.Conv2d(channels[3], channels[2], 3, 1, 1)
        self.conv7_2 = nn.Conv2d(channels[2], channels[2], 3, 1, 1)
        self.conv7_3 = nn.Conv2d(channels[2], channels[2], 3, 1, 1)
        self.conv8_1 = nn.Conv2d(channels[2], channels[1], 3, 1, 1)
        self.conv8_2 = nn.Conv2d(channels[1], channels[1], 3, 1, 1)
        self.conv9_1 = nn.Conv2d(channels[1], channels[0], 3, 1, 1)
        self.conv9_2 = nn.Conv2d(channels[0], channels[0], 3, 1, 1)
        self.conv10 = nn.Conv2d(channels[0], 3, 3, 1, 1)

        self.upsample1 = nn.Upsample(scale_factor=2, mode='nearest')
        self.upsample2 = nn.Upsample(scale_factor=2, mode='nearest')
        self.upsample3 = nn.Upsample(scale_factor=2, mode='nearest')
        self.upsample4 = nn.Upsample(scale_factor=2, mode='nearest')

        self.bn5_3 = nn.BatchNorm2d(channels[4])
        self.bn6_1 = nn.BatchNorm2d(channels[3])
        self.bn6_2 = nn.BatchNorm2d(channels[3])
        self.bn6_3 = nn.BatchNorm2d(channels[3])
        self.bn7_1 = nn.BatchNorm2d(channels[2])
        self.bn7_2 = nn.BatchNorm2d(channels[2])
        self.bn7_3 = nn.BatchNorm2d(channels[2])
        self.bn8_1 = nn.BatchNorm2d(channels[1])
        self.bn8_2 = nn.BatchNorm2d(channels[1])
        self.bn9_1 = nn.BatchNorm2d(channels[0])
        self.bn9_2 = nn.BatchNorm2d(channels[0])

    def forward(self, x):
        # Encoder
        h = F.relu(self.bn1_1(self.conv1_1(x)))
        h = F.relu(self.bn1_2(self.conv1_2(h)))
        h = F.max_pool2d(h, 2)

        h = F.relu(self.bn2_1(self.conv2_1(h)))
        h = F.relu(self.bn2_2(self.conv2_2(h)))
        h = F.max_pool2d(h, 2)

        h = F.relu(self.bn3_1(self.conv3_1(h)))
        h = F.relu(self.bn3_2(self.conv3_2(h)))
        h = F.relu(self.bn3_3(self.conv3_3(h)))
        h = F.max_pool2d(h, 2)

        h = F.relu(self.bn4_1(self.conv4_1(h)))
        h = F.relu(self.bn4_2(self.conv4_2(h)))
        h = F.relu(self.bn4_3(self.conv4_3(h)))
        h = F.max_pool2d(h, 2)

        h = F.relu(self.bn5_1(self.conv5_1(h)))
        h = F.relu(self.bn5_2(self.conv5_2(h)))

        # Decoder
        h = F.relu(self.bn5_3(self.conv5_3(h)))

        h = self.upsample1(h)
        h = F.relu(self.bn6_1(self.conv6_1(h)))
        h = F.relu(self.bn6_2(self.conv6_2(h)))
        h = F.relu(self.bn6_3(self.conv6_3(h)))

        h = self.upsample2(h)
        h = F.relu(self.bn7_1(self.conv7_1(h)))
        h = F.relu(self.bn7_2(self.conv7_2(h)))
        h = F.relu(self.bn7_3(self.conv7_3(h)))

        h = self.upsample3(h)
        h = F.relu(self.bn8_1(self.conv8_1(h)))
        h = F.relu(self.bn8_2(self.conv8_2(h)))

        h = self.upsample4(h)
        h = F.relu(self.bn9_1(self.conv9_1(h)))
        h = F.relu(self.bn9_2(self.conv9_2(h)))

        out = torch.sigmoid(self.conv10(h))

        return out

# For debug
if __name__ == '__main__':
    net = AutoEncoder()

    x = torch.zeros(1, 3, 256, 256, dtype=torch.float, requires_grad=False)
    x_hat = net(x)
    print(x_hat.shape)

    # Using graphviz
    # dot = make_dot(x_hat)
    # dot.format = 'png'
    # dot.render('graph_image')

    # Using onnx
    # input_names = ['input']
    # output_names = ['output']
    # torch.onnx.export(net, x, './model_for_visualize.onnx', verbose=True,
    #                   input_names=input_names,output_names=output_names)

    # Using TensorBoard
    # Command: tensorboard --logdir ./runs
    # http://localhost:6006/
    # writer = SummaryWriter()
    # writer.add_graph(net, x)
    # writer.close()
